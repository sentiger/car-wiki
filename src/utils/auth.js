import Cookies from 'js-cookie'

const TokenKey = 'token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
export function setPermission(permission) {
  return Cookies.set('permission', permission)
}
export function getPermission() {
  return Cookies.get('permission')
}
export function setUsername(username) {
  return Cookies.set('username', username)
}
export function getUsername() {
  return Cookies.get('username')
}
export function removeUsername() {
  return Cookies.remove('username')
}
export function removePermission() {
  return Cookies.remove('permission')
}
export function setRid(rid) {
  return Cookies.set('rid', rid)
}
export function removeRid() {
  return Cookies.remove('rid')
}
export function getRid() {
  return Cookies.get('rid')
}
