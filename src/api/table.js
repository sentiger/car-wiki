import request from '@/utils/request'
import request2 from '@/utils/request2'

export function sendCode(params) {
  return request2({
    url: '/uplod/sendCode',
    method: 'get',
    params
  })
}
export function memberList(params) {
  return request({
    url: '/admin/member/list',
    method: 'get',
    params
  })
}
export function orderList(params) {
  return request({
    url: '/admin/order/list',
    method: 'get',
    params
  })
}
export function driverList(params) {
  return request({
    url: '/admin/driver/list',
    method: 'get',
    params
  })
}
export function carFleetList(params) {
  return request({
    url: '/admin/carFleet/list',
    method: 'get',
    params
  })
}
export function checkCode(params) {
  return request2({
    url: '/uplod/tokenCode',
    method: 'get',
    params
  })
}
export function getList(params) {
  return request({
    url: '/table/list',
    method: 'get',
    params
  })
}
export function getCommunityAll() {
  return request({
    url: '/ws/common/find/zn_communityAll',
    method: 'post',
    data: {
      filter: {

      }
    }
  })
}

export function addCompany(data) {
  return request({
    url: '/ws/common/insertOne/zn_companyList',
    method: 'post',
    data: data
  })
}
export function findCompany(data) {
  return request({
    url: '/ws/common/find/zn_companyList',
    method: 'post',
    data: data
  })
}
export function addCommunity(data) {
  return request({
    url: '/ws/common/insertOne/zn_communityAll',
    method: 'post',
    data: data
  })
}
export function addUserAdmin(data) {
  return request({
    url: '/ws/common/insertOne/zn_login',
    method: 'post',
    data: data
  })
}
export function deleteUserAdmin(data) {
  return request({
    url: '/ws/common/updateOne/zn_login',
    method: 'post',
    data: data
  })
}
export function updateCommunity(data) {
  return request({
    url: '/ws/common/updateOne/zn_communityAll',
    method: 'post',
    data: data
  })
}
export function getUser(data, page, size) {
  return request({
    url: '/ws/common/find/zn_user',
    method: 'post',
    data: {
      filter: data,
      page: page,
      pageSize: size
    }
  })
}
export function getUserCount(data) {
  return request({
    url: '/ws/common/count/zn_user',
    method: 'post',
    data: {
      filter: data
    }
  })
}
export function getChuru(data, page, size) {
  return request({
    url: '/ws/common/find/zn_churu',
    method: 'post',
    data: {
      filter: data,
      page: page,
      pageSize: size
    }
  })
}
export function getChuruCount(data) {
  return request({
    url: '/ws/common/count/zn_churu',
    method: 'post',
    data: {
      filter: data
    }
  })
}
export function updataChuru(data) {
  return request({
    url: '/ws/common/updateOne/zn_churu',
    method: 'post',
    data: data
  })
}
export function updateUser(data) {
  return request({
    url: '/ws/common/updateOne/zn_user',
    method: 'post',
    data: data
  })
}
export function addUserMany(data) {
  return request({
    url: '/ws/common/insertMany/zn_user',
    method: 'post',
    data: data
  })
}

export function getUserImgList(data) {
  return request({
    url: '/ws/common/find/zn_userImgList',
    method: 'post',
    data: {
      filter: data
    }
  })
}
export function updateUserImgList(data) {
  return request({
    url: '/ws/common/updateOne/zn_userImgList',
    method: 'post',
    data: data
  })
}
export function addUserImgList(data) {
  return request({
    url: '/ws/common/insertOne/zn_userImgList',
    method: 'post',
    data: data
  })
}
export function getAttendanceList(data) {
  return request({
    url: '/ws/common/find/zn_attendanceList',
    method: 'post',
    data: {
      filter: data
    }
  })
}
export function getInformationsList(data, page, size) {
  return request({
    url: '/ws/common/find/zn_forward',
    method: 'post',
    data: {
      filter: data,
      page: page,
      pageSize: size
    }
  })
}
export function getInformationsCount() {
  return request({
    url: '/ws/common/count/zn_forward',
    method: 'post',
    data: {
      filter: {}
    }
  })
}
