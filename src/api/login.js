import request from '@/utils/request'
import request2 from '@/utils/request2'

export function login(data) {
  return request2({
    url: '/admin/auth/login',
    method: 'post',
    data: data
  })
}

export function login2(data) {
  return request({
    url: '/ws/common/find/zn_login',
    method: 'post',
    data: {
      filter: data

    }
  })
}
export function setPassword(data) {
  return request({
    url: '/ws/common/updateOne/zn_login',
    method: 'post',
    data: data
  })
}
export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
